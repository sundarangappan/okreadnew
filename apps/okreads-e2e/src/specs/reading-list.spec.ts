import { $, browser, ExpectedConditions } from 'protractor';
import { async } from '@angular/core/testing';

describe('When: I use the reading list feature', () => {
  it('Then: I should see my reading list', async () => {
    await browser.get('/');
    await browser.wait(
      ExpectedConditions.textToBePresentInElement($('tmo-root'), 'okreads')
    );

    const readingListToggle = await $('[data-testing="toggle-reading-list"]');
    await readingListToggle.click();

    await browser.wait(
      ExpectedConditions.textToBePresentInElement(
        $('[data-testing="reading-list-container"]'),
        'My Reading List'
      )
    );
  });
  xit('I should be able to undo', async()=>{
    await browser.get('/');
    await browser.wait(
      ExpectedConditions.textToBePresentInElement($('tmo-root'), 'okreads')
    );

    const readingListToggle = await $('[data-testing="undo-button-container"]');
    await browser.wait(
      ExpectedConditions.textToBePresentInElement(
        $('[data-testing="undo-button-container"]'),
        'Undo'
      )
    );
  })
});
