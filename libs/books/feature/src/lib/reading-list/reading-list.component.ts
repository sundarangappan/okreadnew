import { Component, ChangeDetectorRef } from '@angular/core';
import { Store } from '@ngrx/store';
import { getReadingList, removeFromReadingList, markBookFinished } from '@tmo/books/data-access';

@Component({
  selector: 'tmo-reading-list',
  templateUrl: './reading-list.component.html',
  styleUrls: ['./reading-list.component.scss']
})
export class ReadingListComponent {
  readingList$ = this.store.select(getReadingList);

  constructor(private readonly store: Store, private changeDec : ChangeDetectorRef) {
  }

  removeFromReadingList(item) {
    this.store.dispatch(removeFromReadingList({ item }));
  }

  markFinished(item){
    this.store.dispatch(markBookFinished({item}));
    this.changeDec.detectChanges();
  }
}
