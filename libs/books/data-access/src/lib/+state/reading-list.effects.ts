import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Actions, createEffect, ofType, OnInitEffects } from '@ngrx/effects';
import { of } from 'rxjs';
import {
  catchError,
  concatMap,
  exhaustMap,
  map,
  tap
} from 'rxjs/operators';
import { ReadingListItem } from '@tmo/shared/models';
import * as ReadingListActions from './reading-list.actions';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable()
export class ReadingListEffects implements OnInitEffects {
  loadReadingList$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ReadingListActions.init),
      exhaustMap(() =>
        this.http
          .get<ReadingListItem[]>('/api/reading-list')
          .pipe(
            map((data) =>
              ReadingListActions.loadReadingListSuccess({ list: data })
            )
          )
      ),
      catchError((error) =>
        of(ReadingListActions.loadReadingListError({ error }))
      )
    )
  );

  addBook$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ReadingListActions.addToReadingList),
      concatMap(({ book }) =>
        this.http.post('/api/reading-list', book).pipe(
          map(() => ReadingListActions.confirmedAddToReadingList({ book })),
          tap((data) => {
            this._snackBar.open('Added to Readig List', 'Added', {
              duration: 2000,
            });
          }),
          catchError(() =>
            of(ReadingListActions.failedAddToReadingList({ book }))
          )
        )
      )
    )
  );

  removeBook$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ReadingListActions.removeFromReadingList),
      concatMap(({ item }) =>
        this.http.delete(`/api/reading-list/${item.bookId}`).pipe(
          map(() =>{
            return ReadingListActions.confirmedRemoveFromReadingList({ item })
          }
          
          ),
          tap((data) => {
            this._snackBar.open('Removed form Readig list', 'Removed', {
              duration: 2000,
            });
          }),
          catchError(() =>
            of(ReadingListActions.failedRemoveFromReadingList({ item }))
          )
        )
      )
    )
  );

  markBookFinished$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ReadingListActions.markBookFinished),
      concatMap(({ item }) =>
        this.http.put(`/api/reading-list/${item.bookId}`, item).pipe(
          map(() => {
            return ReadingListActions.confirmedMarkBookFinished({ item })
          }),
          catchError(() =>
            of(ReadingListActions.failedMarkBookFinished({ item }))
          )
        )
      )
    )
  );

  ngrxOnInitEffects() {
    return ReadingListActions.init();
  }

  constructor(
    private actions$: Actions,
    private http: HttpClient,
    private _snackBar: MatSnackBar
  ) {}
}
